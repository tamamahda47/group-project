/**
 * TIDAK SEMUA ADA VALIDASI isNaN
 * operation==1 artinya mencari luas permukaan
 * operation==2 artinya mencari volume
 * utk CONE dan SPHERE ga ada fungsinya disini udah terlanjur buat di task kmren
 */
const index = require('./index.js')

function volumeOfCone(r, h){
    const result=(3.14)*(r**2)*h/3;
    return "Result = "+result;
}
function surfaceAreaOfCone(r,h){
    const result=(3.14)*r*(r + Math.sqrt((r**2) + (h**2)))
    return "Result = "+result
}
// INPUT FOR CONE
function inputRadius(operation) {
    index.rl.question("Radius: ", radius => {
        if (!isNaN(radius)) {
            inputHeight(radius,operation)
        } else {
            console.log(`Radius must be a number`)
            inputRadius(operation)
        }
    })
  }
  function inputHeight(radius,operation){
    index.rl.question("Height ", height => {
      if (!isNaN(height)) {
          if(operation==1){
              console.log(surfaceAreaOfCone(radius,height));
              console.log("======================================");
                index.rl.close()
        }else if(operation==2){
            console.log(volumeOfCone(radius, height))
            console.log("======================================");
                index.rl.close()
          }
         
      } else {
          console.log(`Height must be a number`);
          inputHeight(radius)
      }
  })
  }
 
  module.exports.cone = inputRadius;